# Basic Stripe Donation
Attempt at getting stripe checkout working with drupal in one module. Form will be located at siteurl/donation.


# Concerns
* Not sure if using basic stripe checkout is the right approach.
* I was unable to make this work
  * I couldn't figure out how to correctly reference the library in the src directory and make it work like a plain php form.
  * Adding the form using #allowed_words with #markup is likely unsafe.

## Authors
Chris Purcell at https://gitlab.com/purcell/basic-stripe-test
