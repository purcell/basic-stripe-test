<?php

namespace Drupal\basic_stripe_donation\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\basic_stripe_donation\stripe_php_5_7_0\lib as Stripe;

/**
 * Provides route responses for Basic Stripe Donation module
 */

class DonateController extends ControllerBase
{
  /**
   * Returns a simple page
   * @return array Simple render array.
   */
  public function donate()
  {
    if (isset($_POST['stripeToken'])) {
      $token  = $_POST['stripeToken'];

      $customer = Stripe\Customer::create(array(
          'email' => 'customer@example.com',
          'source'  => $token
      ));

      $charge = Stripe\Charge::create(array(
          'customer' => $customer->id,
          'amount'   => 5000,
          'currency' => 'usd'
      ));

    $template = '<h1>Successfully charged $50.00! with token: ' . $token . '</h1>';
    return array(
      '#markup' => $template
    );
    } else {
      $element = array(
        '#markup' => '
          <h2>General Donation Form</h2>
          <form action="/donation" method="post">
            <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="pk_test_D1vNL7QYUfxNpYQJ6d2THiyU"
                    data-name="test stripe factory"
                    data-description="Donation to our general charity fund."
                    data-amount="5000"
                    data-label="Make a Donation"
                    data-locale="auto">
            </script>
          </form>
        ',
        '#allowed_tags' => ['form', 'script'],
      );
      return $element;
    }
  }
}
